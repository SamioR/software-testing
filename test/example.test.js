const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {

    let myvar = undefined;
    let someValue = undefined;
    before(() => {
        myvar = 1;
        someValue = mylib.arrayGen()[0]
    })
    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1);
        expect(result).to.equal(2);
    })
    it.skip('Assert foo is not bar', () => {
        assert('foo' !== 'bar')
    })
    it('myvar should exist', () => {
        should.exist(myvar)  
    })
    it('should be a some value from range 0 - 1 ', () => {
        const result = mylib.random();
        expect(result).lessThanOrEqual(1);
    })
    it("should be a random item from an array", () => {
        let max = mylib.arrayGen().length;
        let min = 0;
        let index = mylib.floor(mylib.random() * (max - min)) + min;
        const result = mylib.arrayGen()[index];
        expect(mylib.arrayGen()).contains(result);
    })
    after(() => {
        console.log("Hello, this is after function!")
        describe("Value from array", function(){
            it("First value from mylib array", function() {
                someValue.should.equal(mylib.arrayGen()[0]);
            })
        })
        
    })
})

